Computing Environments
----------------------

This was given as a lunch seminar on April 4, 2013.

Note that the run links may not work as well on your PC. I temporarily turned off Adobe's run protection to avoid multiple dialogs.

Use downloads -> tags or branches (above) to get a a download.

To install PyROOT on Windows, just download the windows binary for Root. It will work with the current python only. It was compiled for 2.6, but I've not had trouble with 2.7 (yet). Your environment variables should be set up to make the python version available to root. You also need ``C:\root\bin`` to be in your ``PYTHONPATH`` variable.

To install PyROOT on Ubuntu, you can get the ``root-system`` and ``libroot-bindings-python-dev`` packages. I'm not sure why you need the dev package, but that's where the ``ROOT.py`` entry point is.
