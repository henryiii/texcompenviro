%& -shell-escape
\documentclass[compress, xcolor=dvipsnames, aspectratio=1610]{beamer}
\mode<presentation>

%\usetheme{AnnArbor}
%\usecolortheme[named=Aquamarine]{structure}
\usetheme{progressbar}
%\setbeamertemplate{navigation symbols}{} 

\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage[T1]{fontenc}
\usepackage{units}
\usepackage{textpos}
\usepackage{rotating}
\usepackage{multicol}
\usepackage{listings}

\usepackage{compenvirostyles}

\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}

\definecolor{dgreen}{rgb}{0.,0.6,0.}


\graphicspath{{./images/}}

\title[Computing environments]{Object oriented data handling and analysis\\
                               in different computing environments}
\subtitle{HEP Lunch Seminar}
\author[\insertdate\hspace{3.8cm}Henry Schreiner]{Henry Schreiner$^3$\\
Advisor: Roy Schwitters}
\institute[UT] {
University of Texas \\
\medskip
{
\emph{henryschreineriii@utexas.edu}}
}
\date{\today}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}[noframenumbering]
\begin{multicols}{2}
\tableofcontents
\end{multicols}
\end{frame}

\section[Computing]{Computing environments}
\subsection[History]{History}
\begin{frame}
\frametitle{History of computing environments}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{MATLAB\textsuperscript{\textregistered}}
\begin{itemize}
\item Started in 1984
\item LAPACK core (after rewrite in 2000)
\item Extended with toolboxes
\item Primarily owned and sold by MathWorks\textsuperscript{\textregistered}
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Python\texttrademark}
\begin{itemize}
\item Started in 1991, from ABC background
\item Extended with modules
\end{itemize}
\end{block}
\begin{block}{Numpy}
\begin{itemize}
\item LAPACK based module (1995, 2005)
\item Basis of many modules \\
      (Scipy, Matplotlib)
\end{itemize}
\end{block}
\end{columns}
\end{frame}


\subsection[Comparisons]{Comparing languages}
\begin{frame}
\frametitle{Comparing languages}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{MATLAB}
\begin{itemize}
\item Complete (unless you need a toolbox...)
\item Language optimised for matrices\\
       But sub-par for many other tasks
\end{itemize}
\begin{center}
\mat{y = [1,2; 3,4]};
\end{center}
\end{block}

\column{.5\textwidth}
\begin{block}{Python}
\begin{itemize}
\item Basics, plus many modules
\item Modern, versatile, clean syntax\\
       But matrices are added on
\end{itemize}
\begin{center}
\py{import numpy}

\py{y = numpy.array([[1,2],[3,4]])}
\end{center}
\end{block}
\end{columns}
\end{frame}


\begin{frame}[fragile]
\frametitle{Comparing languages: Arguments}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{MATLAB}
\begin{matlong}
function out = nice(varargin)
p = inputParser;
p.addRequired('x',@isnumeric);
p.addOptional('y',1,@isnumeric);
p.addOptional('z',1,@isnumeric);
p.parse(varargin{:});
p = p.Results;
out = p.x + p.y + p.z;
\end{matlong}
\uncover<2>{Use: \mat{nice(1,'y',2)}}
\end{block}

\column{.5\textwidth}
\begin{block}{Python}
\begin{pylong}
def nice(x, y=1, z=1):
    return x + y + z
\end{pylong}
\uncover<2>{Use: \py{nice(1,y=2)}}
\end{block}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Comparing languages - Speed}
\begin{center}
\py{x=linspace(-1,1,1e7)}
\end{center}
\begin{columns}[c]
\column{.45\textwidth}
\begin{block}{MATLAB}
\mat{y=.25*x.^3+.75*x.^2-1.5*x-2;}\\
Takes 0.81 s
\end{block}

\column{.65\textwidth}
\begin{block}{Python}
\py{y=.25*x**3+.75*x**2-1.5*x-2}\\
Takes 2.71 s

\uncover<2->{\py{y=.25*x*x*x+.75*x*x-1.5*x-2}\\
Takes 1.41 s}

\uncover<3->{\py{y=((.25*x+.75)*x-1.5)*x-2}\\
Takes 0.92 s}

\uncover<4->{\py{y=ne.evaluate('.25*x**3+.75*x**2-1.5*x-2')}
Takes 0.072 s}
\end{block}
\end{columns}
\end{frame}


\subsection[Acceleration]{Accelerating code}
\begin{frame}
\frametitle{Accelerating code}
\begin{block}{Common}
\begin{itemize}
\item Vectorize code, avoid loops
\item GPUs if available - vector or kernel
\item Connect to compiled code
\end{itemize}
\end{block}
\begin{columns}[c]
\column{.45\textwidth}
\begin{block}{MATLAB}
\begin{itemize}
\item Parallel toolbox
\item Microsoft labs
\end{itemize}
\end{block}

\column{.45\textwidth}
\begin{block}{Python}
\begin{itemize}
\item generators
\item numexpr
\item Cython
\end{itemize}
\end{block}
\end{columns}
\end{frame}

\subsection[Connections]{Making connections}
\begin{frame}
\frametitle{Connecting to compiled code}
\begin{columns}[c]
\column{.45\textwidth}
\begin{block}{MATLAB}
\begin{itemize}
\item Connect through mex
\item Significant C boilerplate
\item Deal with it.
\end{itemize}
\end{block}

\column{.43\textwidth}
\begin{block}{Python}
\begin{itemize}
\item Many ways (native C extensions)
\item Native: Significant C boilerplate
\item Not end of story!
\end{itemize}
\end{block}
\end{columns}
\begin{block}{Guidelines}
\begin{itemize}
\item Separate algorithm and connection
\item Identify dependencies to simplify arguments
\end{itemize}
\end{block}
\end{frame}

\section[Good Practices]{Good Practices}
\subsection[Objects]{Object-oriented design}
\begin{frame}[fragile]
\frametitle{Object-oriented methods}
\begin{columns}[c]
\column{.45\textwidth}
\begin{block}{Structured classes}
\begin{itemize}
\item Objects can hold structured data
\item Also hold algorithms
\item Can interact with other objects
\item Use clear commands
\item Inspection, documentation
\item Overrides (add, print, etc.)
\item Inheritance optional
\end{itemize}
\end{block}

\column{.57\textwidth}
\begin{block}{Example}
\begin{pyint}
>>> mydata=dataclass.load('data.dat')
>>> mydata.date()
 Date(1,1,2001)
>>> mydata.setdate(4,4,2013)
>>> mydata.savetotxt('data.txt')
\end{pyint}
\end{block}
\end{columns}
\end{frame}

\subsection[Help]{Writing help}
\begin{frame}[fragile]
\frametitle{Always write help}
\begin{columns}[c]
\column{.29\textwidth}
\begin{block}{Help not optional}
\begin{itemize}
\item Before or during
\item You or others
\item Include example
\item Include equations
\end{itemize}
\end{block}

\begin{block}{Advanced tools}
\begin{itemize}
\item Python: sphinx
\item C++: doxygen
\end{itemize}
\end{block}

\column{.69\textwidth}
\begin{block}{Matlab example (part of class)}
\begin{matlong}
function self = RawData(rawFile, paramFile)
  % Make a new RawData object.
  %  Runs initial calculations automatically.
  %  Optional paramFile can be det number.
  %
  % Example:
  %  x=RawData('det1_testfile.samp.bin',1);
  %  x.plotRawFEB()
\end{matlong}
\end{block}
\end{columns}
\end{frame}


\begin{frame}
\frametitle{Inspection}
\begin{columns}[c]
\column{.25\textwidth}
\begin{block}{Live}
\begin{itemize}
\item Inspection
\item Doc strings
\item Signatures
\end{itemize}
\end{block}
\begin{block}{Static}
\begin{itemize}
\item Doc building
\item IDE inspection
\end{itemize}
\end{block}
\uncover<2>{
\column{.3\textwidth}
\begin{block}{\href{run:matlab.py}{Matlab}}
\begin{itemize}
\item \mat{help functionname}
\item \mat{doc functionname}
\item \mat{Reports}
\end{itemize}
\end{block}
\column{.3\textwidth}
\begin{block}{Python}
\begin{itemize}
\item \py{help(object)} or \py{help(string)}
\item \py{dir(object)}
\item \href{run:ipythongui.py}{iPython}: tab, \py{?}, \py{??}
\item \href{run:spyder.py}{Spyder}: ctrl-space
\end{itemize}
\end{block}
}
\end{columns}
\end{frame}

\subsection[Debug]{Debugging}

\begin{frame}
\frametitle{Nothing works until debugged}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Stepping debugger}
\begin{itemize}
\item \href{run:matlab.py}{Matlab} has graphical debugger
\item \href{run:spyder.py}{Python} has pdb (like gdb)
\item Most IDEs integrate with pdb
\item Also can use logging/print
\end{itemize}
\end{block}
%pudb
\column{.5\textwidth}
\begin{block}{Reasons}
\begin{itemize}
\item Stepping through calculations
\item Work out crashes
\end{itemize}
\end{block}
\end{columns}
\end{frame}



\subsection[Profile]{Profiling and testing}
\begin{frame}
\frametitle{Profiling and tests}
\begin{columns}[c]
\column{.55\textwidth}
\begin{block}{Never optimize until you profile}
\begin{itemize}
\item Runs too slow the first time
\item Discover how long each part takes
\item Rewrite/optimize just slow parts
\item Avoid wasting time coding
\item Stop when acceptable!
\end{itemize}
\end{block}
%pudb
\begin{block}{Profilers}
\begin{itemize}
\item \href{run:matlab.py}{Matlab}: profile button: line by line
\item Python: cProfile
\end{itemize}
\end{block}

\column{.45\textwidth}
\begin{block}{Test first, code later}
\begin{itemize}
\item Write tests first and often
\item After is better than never
\item Tools help write and find tests
\item Can be involved
\item Classic: unittest (new in 2013a)
\item Python: doctest, py.test, nose
\end{itemize}
\end{block}
\end{columns}
\end{frame}


\section[Solving]{Solving problems}
\subsection[Background]{Background of our imaging}
\begin{frame}
\frametitle{Background of our imaging}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Detectors}
\begin{itemize}
\item Made of strips, locating muon tracks
\item Custom code runs on Linux inside
\item Datacubes are 4D histograms
\item Must be able to run anywhere
\end{itemize}
\end{block}
\begin{block}{Tracking code}
\begin{itemize}
\item Real time on detector
\item Also useful in testing
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Projections}
\begin{itemize}
\item Most useful analysis
\item Needs to be usable for scanning
\end{itemize}
\end{block}
\end{columns}
\end{frame}





\subsection[Data and planes]{DataCubes and Planes}
\begin{frame}
\frametitle{DataCubes and Planes}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{\href{run:matlab.py}{Example of Objects}}
\begin{itemize}
\item Load datacubes
\item Optionally combine
\item Make plane
\item Combine to calculate
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{That's not all}
\begin{itemize}
\item Easy to expand
\item \href{run:showsweep.py}{Sweeps allow many planes}
\item \href{run:mlplanes.py}{GUI}
\item Runs on \href{run:matlab.py}{Matlab},  \href{run:ipythonnotebook.py}{Python(beta)}
\end{itemize}
\end{block}
\end{columns}
\end{frame}





\subsection[Detector running]{Detector running software}
\begin{frame}
\frametitle{Detectors run software}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Needs}
\begin{itemize}
\item Any system
\item SSH
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Implementation}
\begin{itemize}
\item Detector connection is class
\item \href{https://maya1.hep.utexas.edu/code/docs/detcon/connection.html}{Documentation} available
\item \href{run:detguirun.py}{GUI} design separate
\end{itemize}
\end{block}
\end{columns}
\end{frame}


\subsection[Extended]{Extended computations}
\begin{frame}
\frametitle{3D volumes}
\begin{columns}[c]
\column{.45\textwidth}
\begin{block}{Need to be flexible}
\begin{itemize}
\item Had to run on supercomputer node
\item \href{run:matlab.py}{Matlab} needed for analysis
\item Needed simple, unified interface
\item Ongoing work
\end{itemize}
\end{block}
\column{.27\textwidth}
\includegraphics[width=\textwidth]{NoRecon1.pdf}
\column{.27\textwidth}
\includegraphics[width=\textwidth]{ReconExample1.pdf}
\end{columns}
\end{frame}


\subsection[Libraries]{Connecting to libraries}

\begin{frame}
\frametitle{Connecting to libraries}
\begin{center}
First, a brief diversion...

\uncover<2>{Let's go back to optimizing in Python}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{Cython}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Basics}
\begin{itemize}
\item Python module
\item Reads and converts to C/C++
\item Standard Python + new keywords
\item Fine grained control
\end{itemize}
\end{block}

\begin{block}{Flexible}
\begin{itemize}
\item Extensions
\item Import hook
\item Inline
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Very simple example}
\begin{pylong}
def fun(int x, double y):
    cdef double z
    z = y**x
    return z+1
\end{pylong}
\end{block}
\begin{block}{Amazing mix}
\begin{itemize}
\item Define in Python or C
\item Toggle Python features
\item Syntax is Python, then C++
\end{itemize}
\end{block}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Connecting to compiled code}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Wrap existing C/C++}
\begin{itemize}
\item Copy and Pythonize headers
\item Templates use \py{[]}
\item Casting uses \py{<>}
\item Some stdlib included
\item Static $\rightarrow$ namespace
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Define Python interface}
\begin{itemize}
\item Write class or functions
\item Make pythonic!
\item Include Documentation!
\item Set keep signatures
\item Build module (distutils, setup.py)
\end{itemize}
\end{block}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Connecting Libraries}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Using C++ libraries}
\begin{itemize}
\item \href{run:spyder.py}{Original Library}
\item Cython header
\item Cython module
\item \href{run:ipythonnotebook.py}{Usage}
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Works more places!}
\begin{itemize}
\item Machinery behind PlaneProject
\item Connects to other libs
\item Fully compatible with numpy
\item Python 3 tool
\end{itemize}
\end{block}
\end{columns}
\end{frame}

\section[Advanced]{Advanced topics}
\subsection[Geant]{Geant simulations}
\begin{frame}
\frametitle{Geant simulations}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{About}
\begin{itemize}
\item Geant is a toolkit
\item Object-oriented inheritance
\item Huge
\item Do-it-yourself philosophy
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Challenges}
\begin{itemize}
\item Building (Now cmake)
\item Connecting to libraries
\item Building geometry
\item Initializing
\item Output
\item Optimizing
\end{itemize}
\end{block}
\end{columns}
\end{frame}




\subsection[ROOT]{ROOT}
\begin{frame}
\frametitle{ROOT}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{About}
\begin{itemize}
\item C++ and interpreted C++
\item Amazing file capabilities
\item Amazingly hard to do simple task
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{\href{run:rootrun.py}{PyROOT}}
\begin{itemize}
\item Use Python's powerful inspection
\item Same syntax or Pythonic
\item Lousy documentation
\item Lousy help
\end{itemize}
\end{block}
\end{columns}
\end{frame}




\subsection[GPU]{Using GPUs}
\begin{frame}
\frametitle{Using GPUs}
\begin{columns}[c]
\column{.5\textwidth}
\begin{block}{Fast at a price}
\begin{itemize}
\item Many stream processors in blocks
\item Block must do the same calculation
\item If statements split calculations!
\item Coded in CUDA kernels
\item Fighting memory (like CPU)
\item Transfer memory penalty
\end{itemize}
\end{block}
\column{.5\textwidth}
\begin{block}{Ways to use}
\begin{itemize}
\item Can compile kernels
\item Can use gpu-array
\item Matlab parallel toolbox
\item PyCUDA (and more)
\end{itemize}
Note: See OpenMP (C, Cython)
\end{block}
\end{columns}
\end{frame}

\begin{frame}
%\frametitle{Questions?}
\begin{center}
Talk/code available on:

\url{http://bitbucket.org/henryiii}

(texcompenviro)
\end{center}
\end{frame}

\end{document}
