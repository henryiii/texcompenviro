import os
from ROOT import TFile, TTree
from numpy import array, zeros
from scipy.io import savemat

readname = 'MarchTestSymAir'

#os.chdir('C:/Geant4/work/HEP')

f = TFile(readname + '.root')

if(f.IsZombie()):
    print 'died'


ft = f.Get('raytree')

#listbr = ft.GetListOfBranches()
#param = ft.GetBranch('param')
#listbrg = param.GetListOfLeaves()
#mom = listbrg.At(0)
#pos = listbrg.At(1)
#nrays = param.GetEntries()

nrays = ft.GetEntries()

firstarr = zeros([nrays,4])
secondarr = zeros([nrays,4])
hit = zeros([nrays],dtype=bool)
madeitout = zeros([nrays],dtype=bool)
outarr = zeros([nrays,4])
starten = zeros([nrays])
hiten = zeros([nrays])
outen = zeros([nrays])
ps = zeros([nrays,3])
xs = zeros([nrays,3])


for i,ell in enumerate(ft):
    firstarr[i] = array(ell.initcoord)
    secondarr[i]= array(ell.finalcoord)
    hit[i] = array(ell.finalhit)
    starten[i] = array(ell.genenergy)
    hiten[i] = array(ell.initenergy)
    outen[i] = array(ell.finalenergy)
    madeitout[i] = array(ell.madeitout)
    outarr[i] = array(ell.endingcoord)
    ps[i] = array(ell.mom)
    xs[i] = array(ell.pos)


f.Close()

print 'Saving...'

savemat(readname,
        {'firstarr':firstarr, 'secondarr':secondarr, 'hit':hit,
         'starten':starten, 'hiten':hiten, 'outen':outen,
         'madeitout':madeitout, 'outarr':outarr,
         'pos':xs, 'mom':ps},
        do_compression=True, oned_as='column')

