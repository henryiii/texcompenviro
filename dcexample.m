data = DataCube.frommat('det4_lab_bricks_6.mat');
back = DataCube.frommat('det4_lab_ffh.mat');

pln = Planes();
pln.sz = [2,2];
pln.res = 5;
pln.center = [0,0,.5];
pln.orientation = eye(3);

pln.project(data,back);
pln.plot()